/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package org.apache.cordova.devicemotion;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AccelListener extends CordovaPlugin implements SensorEventListener {
    private static final int ERROR = -1;
    private static final int STARTED = 0;
    private static final int RUNNING = 1;

    private int status;

    private CallbackContext callbackContext;
    private SensorManager sensorManager;
    private Sensor sensor;

    private int accuracy;

    // CordovaPlugin methods
    @Override
    protected void pluginInitialize() {
        sensorManager = (SensorManager) cordova.getActivity().getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (sensor == null) {
            status = ERROR;
        } else {
            status = STARTED;
        }
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        this.callbackContext = callbackContext;

        if (status == ERROR) {
            sendMessage("No gyroscope available.", status, PluginResult.Status.ERROR, callbackContext);
            return false;
        }

        switch (action) {
            case "start":
                start();
                break;
            case "stop":
                stop();
                break;
            default:
                return false;
        }
        return true;
    }

    @Override
    public void onReset() {
        stop();
    }

    @Override
    public void onResume(boolean multitasking) {
        start();
    }

    @Override
    public void onPause(boolean multitasking) {
        stop();
    }

    @Override
    public void onDestroy() {
        stop();
    }

    // SensorEventListener methods
    @Override
    public void onSensorChanged(SensorEvent event) {
        // If not running, then just return
        if (status != RUNNING) {
            return;
        }

        if (this.accuracy >= SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM) {
            try {
                JSONObject json = new JSONObject();
                json.put("x", event.values[0]);
                json.put("y", event.values[1]);
                json.put("z", event.values[2]);
                json.put("timestamp", event.timestamp);
                sendResult(json, PluginResult.Status.OK, callbackContext);
            } catch (JSONException ignored) {
                // happens if json keys are null
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        this.accuracy = accuracy;
    }

    // helper methods
    private void stop() {
        if (status == RUNNING) {
            sensorManager.unregisterListener(this);
            status = STARTED;
        }
    }

    private void start() {
        if (status == STARTED) {
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_UI);
            status = RUNNING;
        }
    }

    private static void sendMessage(String message, int code, PluginResult.Status status, CallbackContext callbackContext) {
        try {
            JSONObject json = new JSONObject();
            json.put("code", code);
            json.put("message", message);
            sendResult(json, status, callbackContext);
        } catch (JSONException ignored) {
            // happens if json keys are null
        }
    }

    private static void sendResult(JSONObject json, PluginResult.Status status, CallbackContext callbackContext) {
        PluginResult result = new PluginResult(status, json);
        result.setKeepCallback(true);
        callbackContext.sendPluginResult(result);
    }
}
